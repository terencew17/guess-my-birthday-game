# Write a program that guesses the month (1-12) and year
# import randint
# Asks for user name with prompt "Hi! WHat is your name?"
# Guess your month and year with a prompt  Guess «guess number» :
#    «name» were you born in «m» / «yyyy» ? then prompts with "yes or no?"
# Computer respond with "I knew it!" if correct and stops guessing
# Coputer responds with "Drat! Lemme try again!" if user responds no

user_name = input("Hi! What is your name? ")

for guess in range(5):

    from random import randint

    guess_month = randint(1,12)
    guess_year = randint(1924,2004)

    print("Guess", guess + 1, user_name, "were you born in", guess_month, " / ", guess_year, "?")

    answer1 = input("yes or no? ")

    if answer1 == "yes":
        print("I knew it!")
        break
    elif guess < 4:
        print("Drat! Lemme try again!")
print("I have other things to do. Goodbye")
